package keywordsLibrary

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class careTeamAssignandRemove {
	@Keyword
	public void navigateToCareTeamWizardPage(){
		//Navigate to CareTeam Wizard Page
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamWizard'))

		//remove careteam for a existing patient
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamMemberCard'), 5, FailureHandling.OPTIONAL)){
			WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'), 5)
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'))
			WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'), 5)
			WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'))
			WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_UnassigningCareTeamPopupYesButton'), 5)
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_UnassigningCareTeamPopupYesButton'))
		}

	}
	@Keyword
	public void savingtheCareTeam(){
		//saving the care team
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_SaveButton'), 5)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_SaveButton'))
	}
	@Keyword
	public void savingtheAdminApp(){
		//saving the Admin App
		WebUI.click(findTestObject('Object Repository/Application/AdminApp/UpdateUserPage/AA_UUP_SaveButton'))
		WebUI.delay(7)
		WebUI.click(findTestObject('Object Repository/Application/AdminApp/UpdateUserPage/AA_UUP_SaveSuccessfulOKButton'))

		WebUI.switchToDefaultContent()
	}
	@Keyword
	public void clickEnrollmentButton(){
		//navigating and clicking Enrollment button
		WebUI.click(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_EnrollmentButton'), FailureHandling.STOP_ON_FAILURE)
	}
	@Keyword
	public void closingtheCareTeamWizard(){
		//Clicking the Consent Advisory PopUp yes button
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_ConsentAdvisoryPopupYesButton'), 5,FailureHandling.OPTIONAL)){
			WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_ConsentAdvisoryPopupYesButton'),5,FailureHandling.OPTIONAL)
			WebUI.delay(10)
		}

		//Closing the Care Team Wizard
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/EACW_WizardCloseIcon'), 10)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/EACW_WizardCloseIcon'))
		WebUI.switchToDefaultContent()
	}
	@Keyword
	public void verifyAssignedCareTeam(){
		//Navigating and verifying Assigned Care Team
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CareTeamApp/SearchPatientPage/CT_SPP_Button_CareTeamSection'), 5)
		WebUI.scrollToElement(findTestObject('Object Repository/Application/CareTeamApp/SearchPatientPage/CT_SPP_Button_CareTeamSection'), 5)
		WebUI.click(findTestObject('Object Repository/Application/CareTeamApp/SearchPatientPage/CT_SPP_Button_CareTeamSection'))
	}
	@Keyword
	public void removingAssignedCareTeam(){
		//Clicking dot vertical and removing a assigned CareTeam
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'), 10)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'))
	}
	@Keyword
	public void unassignPopUpandClosingWizard(){
		//Clicking dot vertical and removing a assigned CareTeam
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'), 10)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'))
	}
}
