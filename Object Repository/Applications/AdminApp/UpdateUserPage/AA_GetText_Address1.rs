<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_GetText_Address1</name>
   <tag></tag>
   <elementGuidId>7a6295b4-82af-4aae-9fb9-8098237a1cd6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Address 1']//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
