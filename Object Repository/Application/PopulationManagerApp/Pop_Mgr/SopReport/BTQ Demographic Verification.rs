<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTQ Demographic Verification</name>
   <tag></tag>
   <elementGuidId>6f8bfbc6-f2d9-4619-b91c-d4c7511e988b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Patient ID']/following-sibling::td[19]/following::td[3]</value>
   </webElementProperties>
</WebElementEntity>
