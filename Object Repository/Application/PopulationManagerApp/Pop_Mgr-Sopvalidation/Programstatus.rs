<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Programstatus</name>
   <tag></tag>
   <elementGuidId>01f82542-8404-4095-aaf8-9f9563943208</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Program Status']/following::select[1]/option[@value='Completed']</value>
   </webElementProperties>
</WebElementEntity>
