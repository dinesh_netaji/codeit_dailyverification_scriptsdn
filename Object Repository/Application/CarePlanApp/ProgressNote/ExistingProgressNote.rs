<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ExistingProgressNote</name>
   <tag></tag>
   <elementGuidId>0c36a157-0054-4e77-8909-8ccd3b30d0a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='Updated']/following::img[@title='Expand note']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='Updated']/following::img[@title='Expand note']</value>
   </webElementProperties>
</WebElementEntity>
