<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>interventionStatusValue</name>
   <tag></tag>
   <elementGuidId>e22bffda-2cc5-4a98-80e9-11f4b8b7deb0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@id='interventionStatus1']/option[contains(text(),'In Progress')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@id='interventionStatus1']/option[contains(text(),'In Progress')]</value>
   </webElementProperties>
</WebElementEntity>
