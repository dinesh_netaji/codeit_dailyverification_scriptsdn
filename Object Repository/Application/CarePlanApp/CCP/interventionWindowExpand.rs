<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>interventionWindowExpand</name>
   <tag></tag>
   <elementGuidId>925aa81c-cdb6-4c0d-9178-4d8c9a1747c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[contains(text(),&quot;Interventions&quot;)and not(contains(text(),&quot;(0)&quot;))]/preceding-sibling::td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),&quot;Interventions&quot;)and not(contains(text(),&quot;(0)&quot;))]/preceding-sibling::td</value>
   </webElementProperties>
</WebElementEntity>
