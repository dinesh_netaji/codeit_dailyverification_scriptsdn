import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		
		//navigate to demographics app
		test.log(LogStatus.INFO,"Demographics App Navigation")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/DemographicsIcon'), 10, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/DemographicsIcon'), FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(10)
		
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/DemographicsApp/SearchPatient"), 10,FailureHandling.OPTIONAL)
		WebUI.click(findTestObject("Object Repository/Application/DemographicsApp/SearchPatient"),FailureHandling.OPTIONAL)
		
		//Search a patient in demographics
		test.log(LogStatus.INFO,"Search a Patient in Demographics App")
		//send LastName input for patient demog search
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientLastNameInputField'),10).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientLastNameInputField'), patientLastName, FailureHandling.STOP_ON_FAILURE)
		
		//send ID input for patient demog search
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientIDinputField'), 10).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientIDinputField'), patientID, FailureHandling.STOP_ON_FAILURE)
		
		
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_searchButton'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
		
		//select searched patient
		test.log(LogStatus.INFO,"Select Searched Patient")
		TestObject patientSearchObject = findTestObject('Base/commanXpath')
		patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+patientLastName+"']")
		WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)
		WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_selectButton'), FailureHandling.STOP_ON_FAILURE)
		
		//click modify button
		test.log(LogStatus.INFO,"Update Patient Address1 Field")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/ViewPatientPage/DA_VPP_modifyButton'), 10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/ViewPatientPage/DA_VPP_modifyButton'), FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(5)
		
		//update address1 field
		def address1Update = CustomKeywords.'commonutils.commonkeyword.randomTextalone'()
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/Demographics/ModifyPatientPage/DA_MPP_PatientAddress1InputField'),10).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/Demographics/ModifyPatientPage/DA_MPP_PatientAddress1InputField'), address1Update, FailureHandling.STOP_ON_FAILURE)
		
		//save changes
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/ModifyPatientPage/DA_MPP_SaveButton'), FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/ModifyPatientPage/DA_MPP_SaveSuccessfullPopupOkButton'), 10, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/ModifyPatientPage/DA_MPP_SaveSuccessfullPopupOkButton'), FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(5)
										/**Verification in CarePlan App**/
		//Navigate to careplan
		test.log(LogStatus.INFO,"Reflection in CarePlan App Verification")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CarePlan/CarePlanIcon'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/CarePlan/CarePlanIcon'), FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToWindowIndex(1)
		
		WebUI.delay(5)
		
		//Navigation to PatientSearch Field 
		test.log(LogStatus.INFO,"Selected Patient Search")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CarePlan/SearchPatientPage/CP_SPP_patientSearch'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/CarePlan/SearchPatientPage/CP_SPP_patientSearch'), FailureHandling.STOP_ON_FAILURE)
		
		//search a patient
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/CarePlan/SearchPatientPage/CP_SPP_patientID'),5).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/CarePlan/SearchPatientPage/CP_SPP_patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/SearchPatientPage/CP_SPP_lookUP'), FailureHandling.STOP_ON_FAILURE)
		
		//Verify Patient searched patient
		TestObject patientSearchObjectInCarePlan=findTestObject('Base/commanXpath')
		patientSearchObjectInCarePlan.findProperty('xpath').setValue("(//table[@class='dataVal'])[3]//td[contains(text(),'"+patientLastName+"')]")
		WebUI.verifyElementPresent(patientSearchObjectInCarePlan, 15, FailureHandling.STOP_ON_FAILURE)
		
		//verify address1 field update in careplan app
		test.log(LogStatus.INFO,"Verify Address1 of PatientDemog Updated")
		def addressStreet = WebUI.getText(findTestObject('Object Repository/Applications/CarePlan/PatientDemographicsPage/CP_PDP_Get_AddressStreet'),FailureHandling.STOP_ON_FAILURE)
		//WebUI.verifyEqual(address1Update, addressStreet)
		if(println(address1Update) == println(addressStreet)){
			test.log(LogStatus.INFO, "Patient Demographics Address field is successfully")
		}
		
		WebUI.closeWindowIndex(1)
		WebUI.switchToDefaultContent()
		
		WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)
		GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
		test.log(LogStatus.PASS, "TestCase is executed successfully")
}
}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
	throw e
}






